# Desription

Тестовый проект сервиса для обработки заявок. Сервис принимает заявки на получение дебетовых и кредитных карт 

# Getting Started

#### Lombok
для работы приложения в intellij IDEA необходимо установить плагин Lombook и включить annotation processing

# 1й вариант запуска 
в ветке master вариант запуска программы с rabbitmq и postgre в docker контейнерах.  
1) Для запуска выполните docker-compose для этого из корневой ветки, где находится скрипт
выполните команду docker-compose up -d
2) Затем можно запускать само приложение ru.cardsmobile.CardsMobileApp (а так же запустить тесты, например)
3) Если все прошло успешно, на http://localhost:18080/ можно проверить работоспособность приложения.
на ней можно создать заявку на кредитную и дебетовую карту, а так же получить информацию о заявке
Данные для заявок генерирует javaScript. src\main\resources\static\index.html

!!!! Если возникли проблемы смотри 2й вариант запуска

# 2й вариант запуска
переключитесь на ветку allInDocker 

Ветка allInDocker: есть скрипт docker-compose собирающий контейнер с бд и rabbitmq + приложение в контейнере
1) собрать проект скриптом bootJar
2) выполните docker-compose up
3) после запуска перейдите по ссылке http://localhost:32903/ 

Если запустить контейнер в докере не удалось. Переключитеcь на ветку without_docker
и следуйте дальнейшим инструкциям

#### DataBase
В проекте в качестве БД используется postgresql. имя БД cardsMobile
jdbc:postgresql://localhost:5432/cardsMobile
spring.datasource.username=postgres
spring.datasource.password=qwerty$4

Создайте бд с названием cardsMobile, сконфигурируйте файл
src\main\resources\application.properties
укажите host (localhost по умолчанию) а так же имя и пароль пользователя (guest)

При запуске приложения будет выполнен FlyWay скрипт, который инициализирует таблицы базы cardsMobile


#### rabbit 
Rabbit сконфигурирован с параметрами по умолчания
spring.rabbitmq.host=localhost
spring.rabbitmq.port=5672
spring.rabbitmq.username=guest
spring.rabbitmq.password=guest

если у вас параметры отличаются от предсавленных выше, сконфигурируйте файл
src\main\resources\application.properties

#### UI
Для проверки работоспособности создана очень простая HTML страница
на ней можно создать заявку на кредитную и дебетовую карту, а так же получить информацию о заявке
для открытия страницы пройдите по ссылке http://localhost:18080/
Данные для заявок генерирует javaScript. src\main\resources\static\index.html

порт можно сконфигурировать в
src\main\resources\application.properties
по умолчанию server.port=18080

#### Lombok
для работы приложения в intellij IDEA необходимо установить плагин Lombook и включить annotation processing




