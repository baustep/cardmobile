FROM java:8
COPY build/libs/test_service-1.0-SNAPSHOT.jar /usr/src/docker/
WORKDIR /usr/src/docker
EXPOSE 18080
CMD ["java", "-jar", "test_service-1.0-SNAPSHOT.jar"]