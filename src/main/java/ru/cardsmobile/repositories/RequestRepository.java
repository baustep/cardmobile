package ru.cardsmobile.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.cardsmobile.domain.entry.RequestEntry;

public interface RequestRepository extends CrudRepository<RequestEntry, String> {
}
