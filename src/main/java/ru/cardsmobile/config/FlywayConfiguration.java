package ru.cardsmobile.config;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.FluentConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FlywayConfiguration {

    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String password;
    @Value("${spring.datasource.url}")
    private String url;

    @Bean(initMethod = "migrate")
    Flyway flyway() {
        FluentConfiguration configure = Flyway.configure();
        configure.baselineOnMigrate(true);
        configure.dataSource(url, username, password);
        return configure.load();
    }
}
