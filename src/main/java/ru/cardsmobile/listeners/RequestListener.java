package ru.cardsmobile.listeners;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import ru.cardsmobile.domain.dto.DebitRequestDto;
import ru.cardsmobile.domain.dto.RequestDto;
import ru.cardsmobile.services.RequestProcessService;

@Slf4j
@RequiredArgsConstructor
@Component
public class RequestListener {

    private final RequestProcessService requestProcessService;

    @RabbitListener(queues = "${rabbit.queue.name}")
    public void messageProcess(RequestDto requestDto) throws Exception{
        log.debug("message {} recieved ", requestDto);
        Thread.sleep(1000);
        requestProcessService.process(requestDto);
    }
}
