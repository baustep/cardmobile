package ru.cardsmobile.utils;


import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import ru.cardsmobile.domain.dto.CreditRequestDto;
import ru.cardsmobile.domain.entry.CreditRequestEntry;

import java.util.UUID;

@Component
public class CreditConverter implements AbstractConverter<CreditRequestEntry, CreditRequestDto> {
    @Override
    public CreditRequestDto toDto(CreditRequestEntry entry) {
        if (entry == null) {
            return new CreditRequestDto();
        }

        CreditRequestDto dto = CreditRequestDto.builder()
                .ammount(entry.getAmmount())
                .creditInterest(entry.getCreditInterest())
                .build();

        dto.setId(entry.getId());

        return dto;
    }

    @Override
    public CreditRequestEntry toEntry(CreditRequestDto dto) {
        CreditRequestEntry entry;

        if (dto == null) {
            return new CreditRequestEntry();
        }

        if (StringUtils.isEmpty(dto.getId())) {
            dto.setId(UUID.randomUUID().toString());
        }

        entry = new CreditRequestEntry();
        entry.setId(dto.getId());
        entry.setAmmount(dto.getAmmount());
        entry.setCreditInterest(dto.getCreditInterest());

        return entry;
    }
}
