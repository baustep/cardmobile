package ru.cardsmobile.utils;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import ru.cardsmobile.domain.dto.DebitRequestDto;
import ru.cardsmobile.domain.entry.DebitRequestEntry;

import java.util.UUID;

@Component
public class DebitConverter implements AbstractConverter<DebitRequestEntry, DebitRequestDto> {
    @Override
    public DebitRequestDto toDto(DebitRequestEntry entry) {
        if (entry == null) {
            return new DebitRequestDto();
        }

        DebitRequestDto dto = DebitRequestDto.builder()
                .cashBack(entry.getCashBack())
                .overdraft(entry.getOverdraft())
                .build();

        dto.setId(entry.getId());

        return dto;
    }

    @Override
    public DebitRequestEntry toEntry(DebitRequestDto dto) {
        DebitRequestEntry entry;

        if (dto == null) {
            return new DebitRequestEntry();
        }

        if (StringUtils.isEmpty(dto.getId())) {
            dto.setId(UUID.randomUUID().toString());
        }

        entry = new DebitRequestEntry();
        entry.setId(dto.getId());
        entry.setCashBack(dto.getCashBack());
        entry.setOverdraft(dto.getOverdraft());

        return entry;
    }
}
