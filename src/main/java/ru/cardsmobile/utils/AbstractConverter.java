package ru.cardsmobile.utils;



import ru.cardsmobile.domain.dto.RequestDto;
import ru.cardsmobile.domain.entry.RequestEntry;

public interface AbstractConverter<E extends RequestEntry, D extends RequestDto> {

    D toDto(E entry);

    E toEntry(D dto);
}
