package ru.cardsmobile.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import ru.cardsmobile.domain.dto.CreditRequestDto;
import ru.cardsmobile.domain.dto.DebitRequestDto;
import ru.cardsmobile.domain.dto.RequestDto;
import ru.cardsmobile.domain.entry.CreditRequestEntry;
import ru.cardsmobile.domain.entry.DebitRequestEntry;
import ru.cardsmobile.domain.entry.RequestEntry;
import ru.cardsmobile.utils.AbstractConverter;
import ru.cardsmobile.utils.CreditConverter;
import ru.cardsmobile.utils.DebitConverter;

import java.util.function.Consumer;
import java.util.function.Supplier;

@Getter
@Slf4j
public enum Converters {

    DEBIT(CreditConverter::new, CreditRequestDto.class, CreditRequestEntry.class),
    CREDIT(DebitConverter::new, DebitRequestDto.class, DebitRequestEntry.class);

    private AbstractConverter<? extends RequestEntry, ? super RequestDto> converter;
    private Class dtoClazz;
    private Class entryClazz;

    Converters(Supplier<AbstractConverter> converter, Class dtoClazz, Class entryclazz) {
        this.converter = converter.get();
        this.dtoClazz = dtoClazz;
        this.entryClazz = entryclazz;
    }

    public static AbstractConverter getConverter(Class clazz){
        for (Converters converter: Converters.values()) {
            if (converter.getDtoClazz().equals(clazz) || converter.getEntryClazz().equals(clazz)) {
                return converter.getConverter();
            }
        }
        log.debug("can't find converter");
        throw new RuntimeException("Converter not found");
    }
}
