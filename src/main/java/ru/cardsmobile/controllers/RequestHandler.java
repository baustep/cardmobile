package ru.cardsmobile.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.cardsmobile.domain.dto.RequestDto;
import ru.cardsmobile.services.RequestService;

@RestController
@RequestMapping("/handle")
@RequiredArgsConstructor
public class RequestHandler {

    private final RequestService requestService;

    @PostMapping
    public ResponseEntity<String> handle (@RequestBody RequestDto requestDto) {
        return requestService.sendRequest(requestDto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<RequestDto> getbyId(@PathVariable("id") String id) {
        return requestService.getById(id);
    }

}
