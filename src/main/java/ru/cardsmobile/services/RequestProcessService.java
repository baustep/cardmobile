package ru.cardsmobile.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.cardsmobile.domain.dto.RequestDto;
import ru.cardsmobile.domain.entry.RequestEntry;
import ru.cardsmobile.enums.Converters;
import ru.cardsmobile.repositories.RequestRepository;
import ru.cardsmobile.utils.AbstractConverter;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class RequestProcessService {

    private final RequestRepository repository;


    public void process(RequestDto dto) {
        AbstractConverter<? extends RequestEntry, ? super RequestDto> abstractConverter = Converters.getConverter(dto.getClass());
        RequestEntry requestEntry = abstractConverter.toEntry(dto);
        RequestEntry savedRequest = repository.save(requestEntry);
        log.debug("requestEntry with {} id was save. Request = {}", requestEntry.getId(), savedRequest);
    }

    ResponseEntity<RequestDto> getById(String id) {
        log.debug("recieve request with {} id", id);
        Optional<RequestEntry> requestOptional = repository.findById(id);
        RequestDto requestDto = requestOptional.map(this::getRequestDto).orElse(null);

        if (requestDto == null) {
            log.debug("request with {} id wasn't found", id);
            return ResponseEntity.notFound().build();
        } else {
            log.debug("request with {} id was found", id);
            return ResponseEntity.ok(requestDto);
        }

    }

    private RequestDto getRequestDto(RequestEntry requestEntry) {
        AbstractConverter abstractConverter = Converters.getConverter(requestEntry.getClass());
        return abstractConverter.toDto(requestEntry);
    }
}
