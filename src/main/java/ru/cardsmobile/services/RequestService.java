package ru.cardsmobile.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.cardsmobile.domain.dto.RequestDto;

import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class RequestService {

    @Value("${rabbit.queue.name}")
    private String QUEUE;

    private final RabbitTemplate rabbitTemplate;
    private final RequestProcessService requestProcessService;


    public ResponseEntity<String> sendRequest(RequestDto requestDto)  {
        if (StringUtils.isEmpty(requestDto.getId())) {
            requestDto.setId(UUID.randomUUID().toString());
        }
        log.debug("sending");
        rabbitTemplate.convertAndSend(QUEUE, requestDto);

        log.debug("request {} was send to Rabbit with {} id", requestDto, requestDto.getId());
        return ResponseEntity.ok(requestDto.getId());
    }

    public ResponseEntity<RequestDto> getById(String id) {
        return requestProcessService.getById(id);
    }
}
