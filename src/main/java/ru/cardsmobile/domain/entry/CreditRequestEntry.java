package ru.cardsmobile.domain.entry;

import lombok.Data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * @author Stepan Bauer
 * @since 02.03.2019
 *
 * Запрос на кредитную карту
 */
@Entity
@Data
@DiscriminatorValue("credit")
public class CreditRequestEntry extends RequestEntry {

    /** срок кредитования */
    private LocalDateTime creditTerm;

    /** сумма кредитования */
    private Long ammount;

    /** процент по кредиту */
    private short creditInterest;

}
