package ru.cardsmobile.domain.entry;

import lombok.Data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * @author Stepan Bauer
 * @since 02.03.2019
 *
 * Запрос на дебетовую карту
 */
@Entity
@Data
@DiscriminatorValue("debit")
public class DebitRequestEntry extends RequestEntry {

    /** сумма овердрафта */
    private Long overdraft;

    /** сумма кэщбэка на покупки */
    private short cashBack;

    /** признак зарпдатного проекта*/
    private boolean payrollProgramme;

}
