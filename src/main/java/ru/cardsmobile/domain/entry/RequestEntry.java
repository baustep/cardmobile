package ru.cardsmobile.domain.entry;

import lombok.Data;
import javax.persistence.*;


@Entity
@Table(name = "requests")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "db_type")
@Data
public abstract class RequestEntry {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    protected String id;
}
