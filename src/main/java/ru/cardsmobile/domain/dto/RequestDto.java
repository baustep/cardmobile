package ru.cardsmobile.domain.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import ru.cardsmobile.domain.entry.RequestEntry;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "db_type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = CreditRequestDto.class),
        @JsonSubTypes.Type(value = DebitRequestDto.class)
})
public abstract class RequestDto {

    private String id;
}
