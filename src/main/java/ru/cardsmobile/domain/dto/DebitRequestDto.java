package ru.cardsmobile.domain.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.cardsmobile.domain.entry.DebitRequestEntry;
import ru.cardsmobile.domain.entry.RequestEntry;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class DebitRequestDto extends RequestDto{

    private Long overdraft;

    private short cashBack;
}
