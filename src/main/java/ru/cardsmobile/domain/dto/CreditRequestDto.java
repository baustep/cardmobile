package ru.cardsmobile.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class CreditRequestDto extends RequestDto {

    private Long ammount;

    private short creditInterest;
}
