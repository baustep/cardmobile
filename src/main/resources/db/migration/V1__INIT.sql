CREATE TABLE requests
(
  id               VARCHAR(128) PRIMARY KEY,
  overdraft        BIGINT,
  cashBack         SMALLINT,
  payrollProgramme BOOLEAN,
  ammount          BIGINT,
  creditInterest   smallint,
  db_type          VARCHAR(16)
);


