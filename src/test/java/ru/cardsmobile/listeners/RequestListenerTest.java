package ru.cardsmobile.listeners;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;
import ru.cardsmobile.AbstractTest;
import ru.cardsmobile.domain.dto.DebitRequestDto;
import ru.cardsmobile.domain.entry.RequestEntry;
import ru.cardsmobile.repositories.RequestRepository;
import ru.cardsmobile.services.RequestService;

import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class RequestListenerTest extends AbstractTest {

    @Autowired
    RequestService requestService;
    @Autowired
    RequestRepository repository;

    private String id = UUID.randomUUID().toString();

    @Test
    public void listenerTest() throws InterruptedException {
        DebitRequestDto dto = createDebitRequestDro();
        dto.setId(id);

        requestService.sendRequest(dto);
        Thread.sleep(6000);

        Optional<RequestEntry> optionalRequest = repository.findById(id);

        RequestEntry request = optionalRequest.map(this::requestAssert)
                .orElse(null);

        assertThat(request, notNullValue());

    }

    private RequestEntry requestAssert(RequestEntry requestEntry) {
        assertThat(requestEntry.getId(), is(id));
        return requestEntry;
    }
}