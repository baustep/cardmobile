package ru.cardsmobile;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import ru.cardsmobile.domain.dto.CreditRequestDto;
import ru.cardsmobile.domain.dto.DebitRequestDto;

import java.io.IOException;
import java.time.LocalDateTime;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@Rollback
@Transactional
public abstract class AbstractTest {

    protected boolean payrollProgramme = false;
    protected long overdraft = 10_000L;
    protected short cashBack = 5;
    protected LocalDateTime creditTerm = LocalDateTime.now().plusYears(2L);
    protected long ammount = 10_000L;
    protected short creditInterest = 5;


    @Autowired
    protected MockMvc mvc;

    protected <T> T deserialize(String result, Class<T> clazz) {
        try {
            return new ObjectMapper().readValue(result, clazz);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    protected DebitRequestDto createDebitRequestDro() {

        return new DebitRequestDto().toBuilder()
                .overdraft(overdraft)
                .cashBack(cashBack)
                .build();
    }


    protected CreditRequestDto createCreditRequestDro() {

        return new CreditRequestDto().toBuilder()
                .creditInterest(creditInterest)
                .ammount(ammount)
                .build();
    }


}
