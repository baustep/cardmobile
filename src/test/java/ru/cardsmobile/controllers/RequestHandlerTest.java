package ru.cardsmobile.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import ru.cardsmobile.AbstractTest;
import ru.cardsmobile.domain.dto.CreditRequestDto;
import ru.cardsmobile.domain.dto.DebitRequestDto;
import ru.cardsmobile.domain.entry.CreditRequestEntry;
import ru.cardsmobile.domain.entry.DebitRequestEntry;
import ru.cardsmobile.repositories.RequestRepository;


import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class RequestHandlerTest extends AbstractTest {

    @Autowired
    private RequestRepository repository;

    @Test
    public void handleDebitRequest() throws Exception {
        DebitRequestDto dto = createDebitRequestDro();
        String id = UUID.randomUUID().toString();
        dto.setId(id);

        ObjectMapper mapper = new ObjectMapper();
        byte[] content = mapper.writeValueAsBytes(dto);
        MvcResult result = mvc.perform(post("/handle")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        assertThat(response, is(id));
    }

    @Test
    public void handleCreditRequest() throws Exception {
        CreditRequestDto dto = createCreditRequestDro();
        String id = UUID.randomUUID().toString();
        dto.setId(id);

        ObjectMapper mapper = new ObjectMapper();
        byte[] content = mapper.writeValueAsBytes(dto);
        MvcResult result = mvc.perform(post("/handle")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        assertThat(response, is(id));
    }

    @Test
    public void getDebitRequestById() throws Exception {
        String currentId = UUID.randomUUID().toString();
        DebitRequestEntry debitRequestEntry = new DebitRequestEntry();
        debitRequestEntry.setPayrollProgramme(true);
        debitRequestEntry.setOverdraft(overdraft);
        debitRequestEntry.setCashBack(cashBack);
        debitRequestEntry.setId(currentId);

        repository.save(debitRequestEntry);

        MvcResult result = mvc.perform(get("/handle/{id}", currentId))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        DebitRequestDto responseDto = deserialize(content, DebitRequestDto.class);

        assertThat(responseDto, notNullValue());
        assertThat(responseDto.getId(), is(currentId));
        assertThat(responseDto.getCashBack(), is(cashBack));
        assertThat(responseDto.getOverdraft(), is(overdraft));
    }

    @Test
    public void getCreditRequestById() throws Exception {
        String currentId = UUID.randomUUID().toString();
        CreditRequestEntry creditRequestEntry = new CreditRequestEntry();
        creditRequestEntry.setCreditTerm(creditTerm);
        creditRequestEntry.setCreditInterest(creditInterest);
        creditRequestEntry.setAmmount(ammount);
        creditRequestEntry.setId(currentId);

        repository.save(creditRequestEntry);

        MvcResult result = mvc.perform(get("/handle/{id}", currentId))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        CreditRequestDto responseDto = deserialize(content, CreditRequestDto.class);

        assertThat(responseDto, notNullValue());
        assertThat(responseDto.getId(), is(currentId));
        assertThat(responseDto.getCreditInterest(), is(creditInterest));
        assertThat(responseDto.getAmmount(), is(ammount));

    }

}